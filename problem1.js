// Retrieve all the names of the object's properties.
// Return the keys as strings in an array.
// Based on http://underscorejs.org/#keys

function keys(obj) {
  let result = [];

  for (let key in obj) {
    result.push(key);
  }

  return result;
}

module.exports = keys;
