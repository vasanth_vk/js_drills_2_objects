// Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
// http://underscorejs.org/#mapObject

function mapObject(obj, cb) {
  let result = {};

  if (typeof cb != "function") return null;

  for (let key in obj) {
    result[key] = cb(obj[key], key) || obj[key];
  }

  return result;
}

module.exports = mapObject;
