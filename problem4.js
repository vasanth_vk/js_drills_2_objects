// Convert an object into a list of [key, value] pairs.
// http://underscorejs.org/#pairs

function pairs(obj) {
  let result = [];

  for (let key in obj) {
    result.push([key, obj[key]]);
  }

  return result;
}

module.exports = pairs;
