const obj = require("../testObj");
const mapObject = require("../problem3");

console.log(
  mapObject({ start: 5, end: 12 }, function (val, key) {
    return val + 5;
  })
);

console.log(
  mapObject(obj, (val) => {
    return val.length;
  })
);

console.log(mapObject(obj));
