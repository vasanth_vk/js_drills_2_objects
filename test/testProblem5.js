const obj = require("../testObj");
const invert = require("../problem5");

console.log(invert(obj));
console.log(invert({ Moe: "Moses", Larry: "Louis", Curly: "Jerome" }));
