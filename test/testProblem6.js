const obj = require("../testObj");
const defaults = require("../problem6");

var iceCream = { flavor: "chocolate" };
console.log(defaults(iceCream, { flavor: "vanilla", sprinkles: "lots" }));
